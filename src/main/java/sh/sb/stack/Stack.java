package sh.sb.stack;

import java.io.Serializable;
import java.lang.reflect.Array;

/**
 * Created by administrator on 17-09-14.
 */
public class Stack<T> implements Serializable{

        private int maxSize;
        private T[] stackArray;
        private int top;


    public Stack() {
        this(20);
    }

    public Stack(int maxSize){
        this.maxSize = maxSize;
        this.stackArray = (T[]) new Object[maxSize];
        this.top = -1;
    }

        public void push(T j) {
            stackArray[++top] = j;
        }

        public  T pop() {
            return stackArray[top--];
        }

        public  T peek() {
            return stackArray[top];
        }

        public int size() {
            return top + 1;
        }

        public boolean isEmpty() {
            return (top == -1);
        }

        public boolean isFull() {
            return (top == maxSize - 1);
        }


}
