package com.ypfb;

/**
 * Created by administrator on 17-09-14.
 */
public class Stack<T> {

    private T[] stack;
    private int index = 0;
    private int maxLength = 1000;

    public Stack(){
        stack = (T[]) new Object[maxLength];
    }

    public void push(T value){
        stack[index++]=value;
    }
    public T pop(){
        if(index == 0)
            return  null;

        return stack[--index];
    }

    public T tip(){
        if(index == 0)
            return  null;

        return stack[index-1];
    }

    public int size(){
        return  index;
    }

}
