package sh.sb.stack;

import com.ypfb.Stack;
import org.junit.Assert;
import org.junit.Test;

import sh.sb.stack.Stack;

/**
 * Created by administrator on 17-09-14.
 */
public class StackTest {



    @Test
    public void testPush(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);
        int pop = stack.pop();
        Assert.assertTrue( stack.size() == 1);
        Assert.assertTrue( pop == 4);
    }
}
